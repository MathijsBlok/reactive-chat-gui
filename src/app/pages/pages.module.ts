import {NgModule} from '@angular/core';
import {NotFoundComponent} from './not-found/not-found.component';
import {MainComponent} from './main/main.component';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
    NotFoundComponent,
    MainComponent
  ]
})
export class PagesModule {
}
