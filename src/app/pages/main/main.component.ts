import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {guid} from '../../shared/utils/app.utils';
import {HttpClient} from '@angular/common/http';
import {CookieService} from 'ngx-cookie-service';
import {catchError, scan} from 'rxjs/internal/operators';
import {AlertService} from 'ngx-alerts';
import {EMPTY, Observable, Subject} from 'rxjs';
import {webSocket} from 'rxjs/webSocket';
import {Message} from '../../shared/model/message.model';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  private connection$: Subject<Message>;
  private user: string;

  messages$: Observable<Message[]>;

  formGroup = new FormGroup({
    content: new FormControl('', [Validators.required])
  });

  constructor(
    private http: HttpClient,
    private cookieService: CookieService,
    private alertService: AlertService
  ) {
  }

  ngOnInit(): void {
    this.setUser();
    this.setConnection();
    this.setMessages();
  }

  onSubmit(): void {
    if (!this.formGroup.valid) {
      return;
    }
    this.connection$.next({content: this.formGroup.value.content, user: this.user});
    this.formGroup.reset();
  }

  mine(message: Message): boolean {
    return message.user === this.user;
  }

  private setUser() {
    this.user = !!this.cookieService.get('cgi-demo-user-id') ?
      this.cookieService.get('cgi-demo-user-id') :
      this.createId();
  }

  private setConnection() {
    const url = `${location.protocol === 'https:' ? 'wss:' : 'ws:'}//${window.location.host}/ws/messages`;
    this.connection$ = webSocket(url);
  }

  private setMessages() {
    this.messages$ = this.connection$.pipe(
      scan((state: Message[], message: Message) => [message, ...state], []),
      catchError(() => {
        this.alertService.danger('Something went wrong');
        return EMPTY;
      })
    );
  }

  private createId() {
    const user = guid();
    this.cookieService.set('cgi-demo-user-id', user);
    return user;
  }
}
