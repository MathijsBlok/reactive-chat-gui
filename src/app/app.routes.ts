import {Route, RouterModule} from '@angular/router';
import {MainComponent} from './pages/main/main.component';
import {NotFoundComponent} from './pages/not-found/not-found.component';

const ROUTES: Route[] = [
  {
    path: '',
    component: MainComponent
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

export const AppRouterModule = RouterModule.forRoot(ROUTES);
