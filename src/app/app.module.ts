import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {CoreModule} from './core/core.module';
import {PagesModule} from './pages/pages.module';
import {AppRouterModule} from './app.routes';
import {SharedModule} from './shared/shared.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    CoreModule,
    SharedModule,
    PagesModule,
    AppRouterModule
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {
}
