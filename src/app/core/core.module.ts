import {NgModule, Optional, SkipSelf} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AlertModule} from 'ngx-alerts';
import {CookieService} from 'ngx-cookie-service';

@NgModule({
  imports: [
    BrowserAnimationsModule,
    AlertModule.forRoot(),
    NgbModule.forRoot(),
  ],
  providers: [
    CookieService
  ]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error(`CoreModule has already been loaded. Import Core modules in the AppModule only.`);
    }
  }
}
