import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AlertModule} from 'ngx-alerts';
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';

@NgModule({
  exports: [
    CommonModule,
    AlertModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    RouterModule
  ]
})
export class SharedModule { }
